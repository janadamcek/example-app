(function (angular) {
    angular.module('exampleapp', ['ngRoute', 'ng', 'ngResource'])
        .constant("REST_API_PREFIX", 'http://localhost:8080/')
        .config(function ($routeProvider) {
            $routeProvider.when('/config',{
                templateUrl: 'js/tmpl/config.tmpl.html',
                controller: 'ConfigCtrl',
                controllerAs: 'cfgCtrl'
            });

            $routeProvider.otherwise({
                redirectTo: '/'
            });
        })
        .controller('ConfigCtrl', function ($http, REST_API_PREFIX) {
            var _ctrl = this;

            _ctrl.submit = function() {
                $http({
                    method: 'POST',
                    url: REST_API_PREFIX + 'credentials',
                    params:{userName: _ctrl.userName, password: _ctrl.password},
                    headers: {'Access-Control-Allow-Origin': 'http://localhost:8080/credentials'}
                }).then(function (response) {
                    debugger;
                   alert("User " + response.data.userName + " was created.");
                });
            }
        });
})(angular);