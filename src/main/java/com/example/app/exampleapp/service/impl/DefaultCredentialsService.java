package com.example.app.exampleapp.service.impl;

import com.example.app.exampleapp.dao.CredentialsRepository;
import com.example.app.exampleapp.entity.Credentials;
import com.example.app.exampleapp.service.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class DefaultCredentialsService implements CredentialsService {

	@Autowired
	private CredentialsRepository credentialsRepository;

	@Override
	public List<Credentials> getAllCredentials() {
		return StreamSupport.stream(credentialsRepository.findAll().spliterator(), false).collect(Collectors.toList());
	}

	@Override
	public Credentials saveCredentials(String userName, String password) {
		final Credentials credentials = new Credentials();
		credentials.setUserName(userName);
		credentials.setPassword(password);
		return credentialsRepository.save(credentials);
	}

	@Override
	public Credentials changePassword(String userName, String password) throws Exception {
		final Credentials credentials = credentialsRepository.findById(userName).orElse(null);

		if (credentials == null) {
			throw new Exception(String.format("Credentials for user %snot found.", userName));
		}

		credentials.setPassword(password);
		credentialsRepository.save(credentials);
		return credentials;
	}

	@Override
	public void remove(String userName) throws Exception {
		credentialsRepository.deleteById(userName);
	}
}
