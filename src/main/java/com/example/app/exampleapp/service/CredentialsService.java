package com.example.app.exampleapp.service;

import com.example.app.exampleapp.entity.Credentials;

import java.util.List;

public interface CredentialsService {
	List<Credentials> getAllCredentials();

	Credentials saveCredentials(String userName, String password);

	Credentials changePassword(String userName, String password) throws Exception;

	void remove(String userName) throws Exception;
}
