package com.example.app.exampleapp.controllers;

import com.example.app.exampleapp.entity.Credentials;
import com.example.app.exampleapp.service.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CredentialsController {

	@Autowired
	private CredentialsService credentialsService;

	@RequestMapping("/credentials")
	public List<Credentials> getCredentials() {
		return credentialsService.getAllCredentials();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/credentials")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Credentials createCredentials(@RequestParam("userName") String userName, @RequestParam("password") String password) {
		return credentialsService.saveCredentials(userName, password);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/credentials/{userName}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public Credentials changePassword(@PathVariable("userName") String userName, @RequestParam("password") String password) throws Exception {
		return credentialsService.changePassword(userName, password);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/credentials/{userName}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void changePassword(@PathVariable("userName") String userName) throws Exception {
		credentialsService.remove(userName);
	}
}
