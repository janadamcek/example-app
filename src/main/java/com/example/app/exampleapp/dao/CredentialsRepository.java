package com.example.app.exampleapp.dao;

import com.example.app.exampleapp.entity.Credentials;
import org.springframework.data.repository.CrudRepository;

public interface CredentialsRepository extends CrudRepository<Credentials, String> {
}
