(function (angular) {
    angular.module('exampleapp', ['ngRoute', 'ng', 'ngResource', 'ngCookies'])
        .constant("REST_API_PREFIX", '/')
        .constant("TOKEN_KEY", "example.app.token")
        .constant("MEMSOURCE_API_PREFIX", "https://cloud.memsource.com/web/api2/v1/")
        .config(function ($routeProvider) {

            $routeProvider.when('/config', {
                templateUrl: 'js/tmpl/config.tmpl.html',
                controller: 'ConfigCtrl',
                controllerAs: 'cfgCtrl'
            });

            $routeProvider.when('/login', {
                templateUrl: 'js/tmpl/login.tmpl.html',
                controller: 'LoginCtrl',
                controllerAs: 'logCtrl'
            });

            $routeProvider.when('/projects', {
                templateUrl: 'js/tmpl/projects.tmpl.html',
                controller: 'ProjectsCtrl',
                controllerAs: 'projCtrl'
            });

            $routeProvider.otherwise({
                redirectTo: '/config'
            });
        })
        .run(function ($rootScope, $cookies, $location, TOKEN_KEY) {
            $rootScope.$on('$routeChangeStart', function (event, next, current) {
                var route = next && next.$$route ? next.$$route : current && current.$$route ? current.$$route : {};
                if (route.originalPath != '/login' && !$cookies.get(TOKEN_KEY)) {
                    $location.path('/login');
                }
            });
        })
        .controller('LoginCtrl', function ($http, $location, $cookies, MEMSOURCE_API_PREFIX, TOKEN_KEY) {
            var _ctrl = this;

            this.isLoggedIn = !!$cookies.get(TOKEN_KEY);

            _ctrl.submit = function () {
                if ($cookies.get(TOKEN_KEY)) {
                    alert("Already logged in.");
                    return;
                }

                _ctrl.logging = true;
                $http({
                    method: 'POST',
                    url: MEMSOURCE_API_PREFIX + 'auth/login',
                    headers: {
                        "Access-Control-Allow-Origin": '*'
                    },
                    data: {
                        "userName": _ctrl.userName,
                        "password": _ctrl.password
                    }
                }).then(function (response) {
                    _ctrl.logging = false;
                    $cookies.put(TOKEN_KEY, response.data.token);
                    $location.path('/');
                }).catch(function (reason) {
                    _ctrl.logging = false;
                    _ctrl.userName = null;
                    _ctrl.password = null;
                });
            };
        })
        .controller('ConfigCtrl', function ($http, $rootScope, REST_API_PREFIX) {
            var _ctrl = this;
            var _oldCredential = null;

            function _getAllCredentials() {
                _ctrl.loading = true;
                $http({
                    method: 'GET',
                    url: REST_API_PREFIX + 'credentials'
                }).then(function (response) {
                    _ctrl.credentials = response.data;
                    _ctrl.inProcess = false
                    _ctrl.loading = false;
                }).catch(function (reason) {
                    _ctrl.loading = false;
                });
            }

            this.inProcess = false;
            this.loading = false;

            this.submit = function (credential) {
                $http({
                    method: 'POST',
                    url: REST_API_PREFIX + 'credentials',
                    params: {userName: credential.userName, password: credential.password}
                }).then(function (response) {
                    _getAllCredentials();
                });
            };

            this.edit = function (credential) {
                _oldCredential = _.extend({}, credential);
                _ctrl.inProcess = true;
                credential.edit = true;
            };

            this.update = function (credential) {
                $http({
                    method: 'PUT',
                    url: REST_API_PREFIX + 'credentials/' + credential.userName,
                    params: {password: credential.password}
                }).then(function (response) {
                    _ctrl.cancel(credential);
                    _getAllCredentials();
                });
            };

            this.cancel = function (credential) {
                if (credential.new) {
                    _.remove(_ctrl.credentials, credential);
                }

                if(credential.edit){
                    _.extend(credential, _oldCredential);
                }
                _ctrl.inProcess = false;
                credential.new = false;
                credential.edit = false;
            };

            this.createNew = function () {
                _ctrl.inProcess = true;
                _ctrl.credentials.unshift({new: true, userName: null, password: null});
            };

            this.remove = function(credential){
                var r = confirm("Do you realy want to remove credentials for " + credential.userName + "?");
                if (r == true) {
                    $http({
                        method: 'DELETE',
                        url: REST_API_PREFIX + 'credentials/' + credential.userName
                    }).then(function (response) {
                        _getAllCredentials();
                    });
                }
            };

            _getAllCredentials();
        })
        .controller('ProjectsCtrl', function ($http, $cookies, MEMSOURCE_API_PREFIX, TOKEN_KEY) {
            var _ctrl = this;

            this.loading = false;

            function _getAllProjects() {
                _ctrl.loading = true;
                $http({
                    method: 'GET',
                    url: MEMSOURCE_API_PREFIX + 'projects',
                    params: {token: $cookies.get(TOKEN_KEY)}
                }).then(function (response) {
                    _ctrl.projects = response.data.content;
                    _format();
                    _ctrl.loading = false;
                }).catch(function (reason) {
                    _ctrl.loading = false;
                });
            }

            function _format() {
                _.forEach(_ctrl.projects, function (project) {
                    project.targetLangsFormatted = _.join(project.targetLangs, ', ');
                });
            }

            _getAllProjects();
        });
})(angular);