Run MVN application:
1. Run command mvn spring-boot:run in repository root
2. After success sturtup, application can be found on localhost:8080/index.html

Login page
1. It is necessery to log in to account.
2. After successfull log in, application is redirected to config page.

Config page:
1. There should be 2 default records in table.
2. It is possible to add new one or edit old ones. (only password)

Projects page:
1. It should show all user's available projects.